#!/usr/bin/env python
import ROOT
from atlasplots import atlas_style as astyle
from atlasplots import utils
astyle.SetAtlasStyle()
astyle.ATLASLabel(0.2,.87,"Internal")

def ratioplot1(h1, h2, output_filename, ratio_hi, ratio_lo, xaxis_label):
   #ROOT.gROOT.LoadMacro("AtlasStyle.C")   
   #ROOT.SetAtlasStyle()
   astyle.SetAtlasStyle()
   #astyle.ATLASLabel(0.2,.87,"Internal")
   #ROOT.gROOT.LoadMacro("AtlasUtils.C")
   ROOT.gStyle.SetOptStat(0)
   c1 = ROOT.TCanvas("c1", xaxis_label, 600, 600)

   h1.Sumw2()
   h2.Sumw2()
   h1.Scale(1.0 / h1.Integral())
   h2.Scale(1.0 / h2.Integral())
   h1.SetLineColor(ROOT.kBlue+1)
   h1.SetLineWidth(2)
   h2.SetLineColor(ROOT.kRed)
   h2.SetLineWidth(2)
   h1.SetXTitle(xaxis_label)
   h1.SetYTitle("weighted events")
   
   #utils.DrawText(0.2, 0.87,"ATLAS Internal")

   legend = ROOT.TLegend(0.72, 0.75, 0.92, 0.85)
   #legend.AddEntry(h1,"H7 NLO", "l")
   legend.AddEntry(h1, "P8 LO", "l")
   #legend.AddEntry(h2,"H7 LO", "l")
   #legend.AddEntry(h2,"WH NLO","l")
   legend.AddEntry(h2, "P8 NLO","l")
   legend.SetFillStyle(0)
   legend.SetBorderSize(0)
   legend.SetTextSize(0.025)
   
   #astyle.ATLASLabel(0.2,0.87,"Internal")
   #utils.DrawText(0.53, 0.86, "ATLAS", color=2, size=0.05)
   #utils.DrawText(0.7,  0.86, "Internal", color=2, size=0.05)
   
   rp = ROOT.TRatioPlot(h1, h2)
   c1.SetTicks(0, 1)
   rp.SetH2DrawOpt("hist")
   rp.SetSeparationMargin(0)
   rp.Draw()
   # Have to call Draw() first to produce the LowerRefGraph
   rp.GetUpperRefYaxis().SetTitleOffset(1.4)
#   rp.GetUpperRefYaxis().SetTitle("events (a.u.)")
   rp.GetLowerRefGraph().SetMinimum(0.)
   rp.GetLowerRefGraph().SetMaximum(2.)
   rp.GetLowerRefYaxis().SetTitle("ratio")
   rp.GetLowerRefYaxis().SetTitleOffset(1.4)
   rp.GetLowerRefXaxis().SetTitleOffset(1.1)
   rp.GetLowerRefXaxis().SetTitle(xaxis_label)
   rp.GetLowYaxis().SetNdivisions(6)
   rp.GetLowerRefXaxis().SetNdivisions(4)
   rp.SetLeftMargin(0.15)
   rp.SetLowBottomMargin(0.4)
   
   utils.DrawText(0.65, 0.86, "ATLAS", color=1, size=0.03)
   utils.DrawText(0.75,  0.86, "Internal", color=1, size=0.03)
   
   astyle.ATLASLabel=(0.2,0.8,"Internal")
   legend.Draw()
   c1.Update()
   c1.SaveAs("P8_LO_"+output_filename+".pdf")

   raw_input("Press enter to continue")

if __name__== "__main__":
#    p8_file = ROOT.TFile.Open("hist-VBFHbb_aMCNLO_P8.root")
    h7_lo_file = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/hajj_LO_H7_345425_jira/hist-user.cgee.345425.LO_VBFHajj.e6178.truth3deriv_EXT0.root")
    wh_file = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/WH_compare/hist-gridpack_100k.root")
    #h7_file = ROOT.TFile.Open("hist-VBFHbb_aMCNLO_H7_xcuts.root")
    h7_nlo_file = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/mc15_hajj_jira_all_evgen_hists/hist-user.cgee.345485.VBFHajj.e7663.truth3deriv_EXT0.root")
#    lo_file = ROOT.TFile.Open("June2inputs/Hajj_lo_P8.root")
    p8_nlo_file = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/P8_100k_NLO_isocuts/hist-Hajj_P8_isocuts_DAOD.root")
    p8_lo_file = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/P8_LO_Hajj/hist-mc16_13TeV.root")

    histos_to_compare = [['h_higgs_pT', 'Higgs transverse momentum [GeV]'],
                         ['h_nJets', 'number of jets'],
                         ['h_j1_pT', 'j1 transverse momentum [GeV]'],
                         ['h_b1_phi', 'leading bjet phi'], 
                         ['h_b1b2_mass', 'bb invariant mass [GeV]'],
                         ['h_j1j2_mass', 'jj invariant mass [GeV]'],
                         ['h_j1j2_dEta', '#Delta #eta jj'],
                         ['h_photons_pT', 'photon pT [GeV]'],
                         ['h_nPhotons', 'number of photons'],
                         ['h_pTbalance', 'pT balance'],
                         ['h_centrality', 'photon centrality'],
                         ['h_zj3', 'Rainwater-Zeppenfeld variable']]
    for (histo_name, xaxislabel) in histos_to_compare:
        h1 = p8_lo_file.Get(histo_name)
        h2 = p8_nlo_file.Get(histo_name)
        ratioplot1(h1, h2, 'ratio'+histo_name, 2.0, -0.5, xaxislabel)
        
       
