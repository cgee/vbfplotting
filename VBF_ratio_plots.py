#!/usr/bin/env python
import ROOT
from atlasplots import atlas_style as astyle
from atlasplots import utils
astyle.SetAtlasStyle()
astyle.ATLASLabel(0.2,.87,"Internal")

def ratioplot1(h1, h2, output_filename, ratio_hi, ratio_lo, xaxis_label):
   #ROOT.gROOT.LoadMacro("AtlasStyle.C")   
   #ROOT.SetAtlasStyle()
   astyle.SetAtlasStyle()
   #astyle.ATLASLabel(0.2,.87,"Internal")
   #ROOT.gROOT.LoadMacro("AtlasUtils.C")
   ROOT.gStyle.SetOptStat(0)
   c1 = ROOT.TCanvas("c1", xaxis_label, 600, 600)

   h1.Sumw2()
   h2.Sumw2()
   h1.Scale(1.0 / h1.Integral())
   h2.Scale(1.0 / h2.Integral())
   #h3.Scale(1.0 / h3.Integral())
   h1.SetLineColor(ROOT.kBlue+1)
   h1.SetLineWidth(2)
   h2.SetLineColor(ROOT.kRed)
   h2.SetLineWidth(2)
   #h3.SetLineColor(ROOT.kGreen)
   #h3.SetLineWidth(2)
   h1.SetXTitle(xaxis_label)
   h1.SetYTitle("weighted events")
   
   #utils.DrawText(0.2, 0.87,"ATLAS Internal")

   legend = ROOT.TLegend(0.72, 0.75, 0.92, 0.85)
   #legend.AddEntry(h1,"H7 NLO Hajj", "l")
   #legend.AddEntry(h1, "H7 NLO Hajj Zmass", "l")
   #legend.AddEntry(h2,"H7 LO", "l")
   #legend.AddEntry(h2,"WH NLO","l")
   #legend.AddEntry(h1, "P8 LO","l")
   #legend.AddEntry(h2, "P8 NLO","l")
   #legend.AddEntry(h2, "P8 LO Dipole Recoil on")
   legend.AddEntry(h1, "nonResQCD old rucio")
   legend.AddEntry(h2, "nonResQCD new production")
   #legend.AddEntry(h1, "P8 LO bbjja No Dip Rec")
   #legend.AddEntry(h2, "H7 NLO Zajj")
   #legend.AddEntry(h1, "H7 NLO HS-05")
   #legend.AddEntry(h2, "H7 NLO HS-2")
   legend.SetFillStyle(0)
   legend.SetBorderSize(0)
   legend.SetTextSize(0.025)
   
   #astyle.ATLASLabel(0.2,0.87,"Internal")
   #utils.DrawText(0.53, 0.86, "ATLAS", color=2, size=0.05)
   #utils.DrawText(0.7,  0.86, "Internal", color=2, size=0.05)
   
   rp = ROOT.TRatioPlot(h1, h2)
   c1.SetTicks(0, 1)
   rp.SetH2DrawOpt("hist")
   rp.SetSeparationMargin(0)
   rp.Draw()
   # Have to call Draw() first to produce the LowerRefGraph
   rp.GetUpperRefYaxis().SetTitleOffset(1.4)
#   rp.GetUpperRefYaxis().SetTitle("events (a.u.)")
   rp.GetLowerRefGraph().SetMinimum(0.)
   rp.GetLowerRefGraph().SetMaximum(2.)
   rp.GetLowerRefYaxis().SetTitle("ratio")
   rp.GetLowerRefYaxis().SetTitleOffset(1.4)
   rp.GetLowerRefXaxis().SetTitleOffset(1.1)
   rp.GetLowerRefXaxis().SetTitle(xaxis_label)
   rp.GetLowYaxis().SetNdivisions(6)
   rp.GetLowerRefXaxis().SetNdivisions(4)
   rp.SetLeftMargin(0.15)
   rp.SetLowBottomMargin(0.4)
   
   utils.DrawText(0.65, 0.86, "ATLAS", color=1, size=0.03)
   utils.DrawText(0.75,  0.86, "Internal", color=1, size=0.03)
   
   astyle.ATLASLabel=(0.2,0.8,"Internal")
   legend.Draw()
   c1.Update()
   c1.SaveAs("nonResQCD_productions"+output_filename+".pdf")

   raw_input("Press enter to continue")

if __name__== "__main__":
    #p8_file = ROOT.TFile.Open("hist-VBFHbb_aMCNLO_P8.root")
    h7_lo_file = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/LO_H7_jetoverlapremoval2/hist-H7_user.cgee.345425.LO_VBFHajj.e6178.truth3deriv_EXT0.root")
    wh_file = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/WH_compare/hist-gridpack_100k.root")
    #h7_file = ROOT.TFile.Open("hist-VBFHbb_aMCNLO_H7_xcuts.root")
    h7_nlo_file = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/H7_NLO_j5/hist-user.cgee.345485.VBFHajj.e7663.truth3deriv_EXT0.root")
#    lo_file = ROOT.TFile.Open("June2inputs/Hajj_lo_P8.root")
    p8_nlo_file = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/NLO_P8_jetoverlapremoval2/hist-Hajj_P8_isocuts_DAOD.root")
    p8_lo_file = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/P8_LO_noDipRec_j5/hist-P8_mc16_13TeV.root")
    p8_lo_dipRec_file = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/P8_LO_dipRec_j5/hist-DAODs.root")
    #non Res QCD input files
    p8_lo_nonRes_qcd_file = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/P8_LO_rucio_344180_970kevents/hist-mc16_13TeV.root")
    p8_lo_nonRes_qcd_dipRec_file = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/nonRes_QCD_dipRec_600k/hist-DAOD.root")
    h7_nlo_hs05_file = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/Hajj_hardscale05_200k/hist-DAODs.root")
    h7_nlo_hs2_file = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/Hajj_hardscale2_200k/hist-DAODs.root")
    wh_file = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/WH_compare/hist-gridpack_100k.root")
    zajj_nlo_file = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/zajj_nlo_nophotonoverlap/hist-DAODs.root")
    zajj_5k = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/Zajj_5k/hist-gridpack.root")
    hajj_zmass = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/hajj_zmass_nophotonoverlap/hist-gridpack.root")
    zajj_photonoverlapremov = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/zajj_nlo_photonoverlapRemoval11-5_7/hist-DAODs.root")
    nonResQCD_photonoverlap = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/nonResQCD_dipRec_600k_400mjj/hist-DAOD.root")
    nonResQCD_oldfilter = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/nonResQCD_344180_oldfilter/hist-mc16_13TeV.root")
    nonResQCD_newfilter = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/nonResQCD_newFilters/hist-nonResQCD_newFilters_EVNTfiles.root")
    nonResQCD_oldrucio = ROOT.TFile.Open("/export/home/cgee/vbfgtruth_ntuple/vbfgtruth/run/nonResQCD_full344180_rucio/hist-user.cgee.344180.bbjjaNonResQCDtest4.e5163.truth3deriv_EXT0.root")
    nonResQCD_newproduction = ROOT.TFile.Open("/export/home/cgee/nonResQCD_pandas/hist-newproduction1-17-daods.root")

    histos_to_compare = [['h_higgs_pT', 'Higgs transverse momentum [GeV]'],
                         ['h_nJets', 'number of jets'],
                         ['h_j1_pT', 'j1 transverse momentum [GeV]'],
                         ['h_j2_pT', 'j2 transverse momentum [GeV]'],
                         ['h_j3_pT', '3rd Jet pT [GeV]'],
                         #['h_b1_pt', 'leading bjet pt'], 
                         ['h_b1b2_mass', 'bb invariant mass [GeV]'],
                         ['h_b1b2_mass_smeared', 'smeared bb invariant mass [GeV]'],
                         ['h_b1b2_pT', 'b1b2 pT [GeV]'],
                         ['h_b1b2_deltaR', 'b1b2 deltaR [GeV]'],
                         ['h_j1j2_mass', 'jj invariant mass [GeV]'],
                         ['h_j1j2_dEta', '#Delta #eta jj'],
                         ['h_photons_pT', 'photon pT [GeV]'],
                         ['h_nPhotons', 'number of photons'],
                         ['h_pTbalance', 'pT balance'],
                         ['h_centrality', 'photon centrality'],
                         ['h_zj3', 'Rainwater-Zeppenfeld variable']]
    
    
    for (histo_name, xaxislabel) in histos_to_compare:
        h1 = nonResQCD_oldrucio.Get(histo_name)
        h2 = nonResQCD_newproduction.Get(histo_name)
        ratioplot1(h1, h2, 'ratio'+histo_name, 2.0, -0.5, xaxislabel)
        
       
