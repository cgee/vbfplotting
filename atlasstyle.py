from __future__ import absolute_import, division, print_function

import ROOT
import numpy as np
from atlasplots import atlas_style as astyle
from atlasplots import utils


def plots(h1,h2, output_filename,xaxislabel):
    astyle.SetAtlasStyle()
    ROOT.gStyle.SetOptStat(0)


    c1 =ROOT.TCanvas("c1","hists",800,600);

#setup pad 1
    pad1 = ROOT.TPad("pad1","pad1", 0, 0.3, 1, 1.0)
    pad1.SetBottomMargin(0.02)
    pad1.Draw()
    c1.cd()
    
    pad2 = ROOT.TPad("pad2", "pad2", 0, 0, 1, 0.3)
    pad2.SetTopMargin(0.03)  # joins upper and lower plot
    pad2.SetBottomMargin(0.2)
    pad2.Draw()
    pad1.cd()

#trying to set yaxis range    
    h1.Sumw2()
    h2.Sumw2()
    h1.Scale(1.0 / h1.Integral())
    h2.Scale(1.0 / h2.Integral())
    
    max_val = 1.4*h1.GetMaximum()
    min_val = .9*h1.GetMinimum()
    h1.SetMaximum(max_val)
    h1.SetMinimum(min_val)

    h1.GetYaxis().SetTitleOffset(1.8)
    h1.GetYaxis().SetTitle("Weighted MC Events")
    h1.GetYaxis().SetTitleSize(20)
    h1.GetYaxis().SetLabelFont(43)
    h1.GetYaxis().SetLabelSize(18)
    h1.GetYaxis().SetTitleFont(43)
    h1.GetXaxis().SetLabelSize(0)
    

#Plot specifics
    h1.SetMarkerColor(4)
    h1.SetLineWidth(2)
    h1.SetLineColor(4)
    h1.SetMarkerSize(0)
    h1.Draw("hist")

    h2.SetLineWidth(2)
    h2.SetLineColor(2)
    h2.SetMarkerColor(2)
    h2.SetMarkerSize(0)
    #h1_smeared.SetMaximum(max_val)
    #h1_smeared.SetMinimum(min_val)
    h2.Draw("hist same")

    legend = ROOT.TLegend(0.55,0.78,0.9,0.93)
    legend.AddEntry(h1,"Hard Scale Factor x=1")
    legend.AddEntry(h2,"Hard Scale Factor x=0.5")
    legend.SetFillStyle(0)
    legend.SetBorderSize(0)
    legend.SetTextSize(0.04)
    legend.Draw()

    astyle.ATLASLabel(0.2, 0.87, "Internal")
    utils.DrawText(0.2, 0.80, "#sqrt{s} = 13 TeV")
    pad2.cd()

#Define the ratio plot
    ratio = h1.Clone("h3")
    ratio.SetLineColor(1)
    ratio.SetMinimum(0.5)#  // Define Y ..
    ratio.SetMaximum(1.5)# // .. range
    ratio.SetStats(0)  #    // No statistics on lower plot
    ratio.Divide(h2)
    ratio.SetMarkerStyle(21)
    ratio.SetMarkerColor(1)
    ratio.SetMarkerSize(1)
    ratio.GetXaxis().SetTitle(xaxislabel)

    ratio.Draw("ep") # ep      // Draw the ratio plot

# Set axis titles
    ratio.SetLineColor(1)
    ratio.GetYaxis().SetTitle("ratio")
#ratio.GetYaxis().SetNdivisions(8)
    ratio.GetYaxis().SetTitleSize(20)
    ratio.GetYaxis().SetTitleFont(43)
    ratio.GetYaxis().SetTitleOffset(1.55)
    ratio.GetYaxis().SetLabelFont(43) # Absolute font size in pixel (precision 3)
    ratio.GetYaxis().SetLabelSize(15)
#x  axis ratio plot settings
    ratio.GetXaxis().SetTitleSize(16)
    ratio.GetXaxis().SetTitleFont(43)
    ratio.GetXaxis().SetTitleOffset(3.5)
    ratio.GetXaxis().SetLabelFont(43) # Absolute font size in pixel (precision 3)
    ratio.GetXaxis().SetLabelSize(19)


 #add line at 1 on ratio plot
    xmin = ratio.GetXaxis().GetXmin()
    xmax = ratio.GetXaxis().GetXmax()
    l = ROOT.TLine(xmin, 1, xmax, 1)
    l.SetLineStyle(2)
    l.Draw("same")

    c1.Draw()
    c1.SaveAs("H7_NLO_HS05_"+output_filename+".pdf")
    c1.SaveAs("H7_NLO_HS05_"+output_filename+".png")







if __name__== "__main__":

    h7_nlo_file = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/H7_NLO_j5/hist-user.cgee.345485.VBFHajj.e7663.truth3deriv_EXT0.root")

    h7_nlo_hs05_file = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/Hajj_hardscale05_200k/hist-DAODs.root")
    h7_nlo_hs2_file = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/Hajj_hardscale2_200k/hist-DAODs.root")            
            
            
    nonResQCD_newproduction = ROOT.TFile.Open("/gpfs/slac/atlas/fs1/u/cagee/vbfgtruth/run/nonResQCD_scratch7_rebinpta_1-31-2020/hist-copy_daods.root")
    nonResQCD_oldrucio_pta35 = ROOT.TFile.Open("/gpfs/slac/atlas/fs1/u/cagee/nonResQCD_old_pta35_mjj900_2m/hist-user.cgee.344180.bbjjaNonResQCDtest4.e5163.tr\
uth3deriv_EXT0.root")
    nonResQCD_newproduction_pta35 = ROOT.TFile.Open("/gpfs/slac/atlas/fs1/u/cagee/vbfgtruth/run/nonResQCD_new_pta35_mjj900_1m/hist-copy_daods.root")
    nonResQCD_344180_2020 = ROOT.TFile.Open("/gpfs/slac/atlas/fs1/u/cagee/vbfgtruth/run/nonResQCD_344180_2020_50mil/hist-daods.root")
    nonResQCD_resmear_344180_2020 = ROOT.TFile.Open("/gpfs/slac/atlas/fs1/u/cagee/vbfgtruth/run/nonResQCD_addBDTsmearing/hist-daods.root")

    histos_to_compare = [#['h_higgs_pT', 'Higgs transverse momentum [GeV]'],
                         #['h_nJets', 'number of jets'],
                         #['h_j1_pT', 'j1 transverse momentum [GeV]'],
                         #['h_j2_pT', 'j2 transverse momentum [GeV]'],
                         #['h_j3_pT', '3rd Jet pT [GeV]'],
                         #['h_b1_pt', 'leading bjet pt'],
                         ['h_b1b2_mass', 'bb invariant mass [GeV]'],
                         ['h_b1b2_mass_smeared', 'smeared bb invariant mass [GeV]'],
                         ['h_b1b2_pT', 'b1b2 pT [GeV]'],
                         ['h_b1b2_deltaR', 'b1b2 deltaR [GeV]'],
                         ['h_j1j2_mass', 'jj invariant mass [GeV]'],
                         #['h_j1j2_dEta', '#Delta #eta jj'],
                         #['h_photons_pT', 'photon pT [GeV]'],
                         #['h_nPhotons', 'number of photons'],
                         #['h_pTbalance', 'pT balance'],
                         #['h_centrality', 'photon centrality'],
                         ['h_zj3', 'Rainwater-Zeppenfeld variable']]


    for (histo_name, xaxislabel) in histos_to_compare:
        h1 = h7_nlo_file.Get(histo_name)
        h2 = h7_nlo_hs05_file.Get(histo_name)
        plots(h1, h2, 'ratio'+histo_name, xaxislabel)
