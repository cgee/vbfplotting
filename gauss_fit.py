import ROOT

h7_lo_file = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/hajj_LO_H7_345425_jira/hist-user.cgee.345425.LO_VBFHajj.e6178.truth3deriv_EXT0.root")
h7_nlo_file = ROOT.TFile.Open("/export/home/cgee/vbfgtruth/run/mc15_hajj_jira_all_evgen_hists/hist-user.cgee.345485.VBFHajj.e7663.truth3deriv_EXT0.root")
lo_histo = h7_lo_file.Get("h_b1b2_mass")
nlo_histo = h7_nlo_file.Get("h_b1b2_mass")

new_lo = lo_histo.Clone()
new_nlo = nlo_histo.Clone()
print "cloned"

#new_lo.SetDirectory(0)

can_lo = ROOT.TCanvas()
new_lo.Scale(1.0 / new_lo.Integral())
new_nlo.Scale(1.0 / new_nlo.Integral())
new_lo.Draw()
new_nlo.SetLineColor(ROOT.kBlue+1)
new_nlo.Draw("same")
f1 = ROOT.TF1("f1","[0]*TMath::Landau(-x,-1*[1],[2])",50,150)
f1.SetParameters(.2,125,1)
#f2 = ROOT.TF1("f2","TMath::crystalball(x,1,1,125,1)",50,150)
f3 = ROOT.TF1("f3","[0]*TMath::Landau(-x,-1*[1],[2])",50,150)
f3.SetLineColor(ROOT.kGreen)
f3.SetParameters(.2,125,1)
new_lo.Fit("f1")
new_nlo.Fit("f3")
can_lo.Draw()
can_lo.Update()
can_lo.SaveAs("gaussfit_lo.pdf")
